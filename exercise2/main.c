/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/3/2020
 */

#include <stdio.h>
#include <string.h>

int conta(char s[20], int n);
int inArray(char haystack[], char needle, int length);

int main(int argc, char* argv[]) {
    int length;
    int linesNumber = 0;
    char tmpLine[20];
    int c = 0;
    int totalCount = 0;
    int tmpConta;

    if (argc != 2) {
        printf("Invalid arguments number.");
        return -1;
    }

    printf("Insert the substrings length: ");

    if (scanf("%d", &length) != 1) {
        printf("Invalid integer. Exiting...");
        return -1;
    }

    FILE* fp = fopen(argv[1], "r");

    if (fp == NULL) {
        printf("Invalid file. Exiting...");
        return -1;
    }

    fscanf(fp, "%d", &linesNumber);

    while (!feof(fp) && c < linesNumber) {
        fscanf(fp, "%s", tmpLine);

        tmpConta = conta(tmpLine, length);

        if (tmpConta > 0) {
            totalCount = totalCount + tmpConta;
        }

        c ++;
    }

    printf("%d substrings found!", totalCount);
    return 0;
}

int conta(char s[20], int n) {
    if (n > strlen(s) || n < 0) {
        return -1;
    }

    char tmpValue;
    char vowels[5] = {'a', 'e', 'i', 'o', 'u'};
    int tmpVowelsNumber = 0;
    int counter = 0;

    for (int i = 0; i <= strlen(s) - n; i++) {
        tmpVowelsNumber = 0;
        for (int c = 0; c < n; c++) {
            tmpValue = s[i+c];
            if (inArray(vowels, tmpValue, 5)) {
                tmpVowelsNumber ++;
            }
        }

        if (tmpVowelsNumber == 2) {
            counter ++;
        }
    }

    return counter;
}

// Does needle exist in the haystack?
int inArray(char haystack[], char needle, int length) {
    for (int i = 0; i < length; i++) {
        if (haystack[i] == needle) {
            return 1;
        }
    }
    return 0;
}
