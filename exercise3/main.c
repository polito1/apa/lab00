/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 3/10/2020
 */
#include <stdio.h>
#define maxN 30

void ruota(int v[maxN], int n, int p, int dir);
void printArr(int v[maxN], int n);

int main() {
    int N;
    int V[maxN];
    int P;
    int dir;

    printf("Insert the number of array's cells (max %d): ", maxN);
    scanf("%d", &N);

    for (int i = 0; i < N; i++) {
        printf("Insert the value for the cell number %d: ", i);
        scanf("%d", &V[i]);
    }

    printf("Insert the number of rotations: ");
    scanf("%d", &P);

    printf("Insert the direction (-1 for right shift, 1 for left shift): ");
    scanf("%d", &dir);

    ruota(V, N, P, dir);

    printArr(V, N);
    return 0;
}

void ruota(int v[maxN], int n, int p, int dir) {
    if (p == 0) {
        return;
    }

    int tmpValue;

    // Right shift
    if (dir == -1) {
        tmpValue = v[n-1];
        for (int i = n-1; i >= 0; i--) {
            v[i] = v[i-1];
        }
        v[0] = tmpValue;
    }
    // Left shift
    else {
        tmpValue = v[0];
        for (int i = 0; i < n; i++) {
            v[i] = v[i+1];
        }
        v[n-1] = tmpValue;
    }

    ruota(v, n, p - 1, dir);
}

void printArr(int v[maxN], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d\t", v[i]);
    }
}