/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/02/2020
 */

#include <stdio.h>
#define N 30

void printArray(int arr [], int length);

int main() {
    int count = 0;
    int array[N+1] = {0};
    int maxStep = -1;
    int tmpArr[N] = {0};
    int step = 0;

    printf("Insert the value and press enter (insert any non-digit value to end); max %d values: ", N);

    while (count < N && scanf("%d", &array[count]) == 1) {
        count++;
    }

    printf("The array is: ");
    printArray(array, count);
    printf("\n");

    for (int c = 0; c <= count; c++) {
        if (array[c] != 0) {
            step ++;
        } else {
            if (step > maxStep) {
                maxStep = step;
            }
            step = 0;
        }
    }

    printf("The sub-array(s) with the max length(s) is(are): ");
    for (int i = 0, s = 0; i <= count; i++) {
        if (array[i] == 0 || i == count) {
            if (s == maxStep) {
                printArray(tmpArr, s);
                printf("\t");
            }
            s = 0;
        } else {
            tmpArr[s] = array[i];
            s++;
        }
    }
}

void printArray(int arr[], int length) {
    printf("[ ");
    for (int i = 0; i < length; i++) {
        printf("%d ", arr[i]);
    }
    printf("]");
}
